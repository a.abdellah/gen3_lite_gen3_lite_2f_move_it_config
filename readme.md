
on commence par lancer le model en RVIZ avec MoveIt, en utilisant la commande suivante::
    "roslaunch gen3_lite_gen3_lite_2f_move_it_config demo.launch"

ensuite le code python pour calculer IK par la commande suivante:
    "rosrun gen3_lite_gen3_lite_2f_move_it_config moveit_ik_demo.py"

