#!/usr/bin/env python

import rospy, sys
import moveit_commander
from moveit_msgs.msg import RobotTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint

from geometry_msgs.msg import PoseStamped, Pose
from tf.transformations import euler_from_quaternion, quaternion_from_euler

class MoveItDemo:
    def __init__(self):
        # Initialize the move_group API
        moveit_commander.roscpp_initialize(sys.argv)
        
        rospy.init_node('moveit_demo')
                
        # Initialize the move group for the right arm
        arm = moveit_commander.MoveGroupCommander('arm')
                
        # Get the name of the end-effector link
        end_effector_link = arm.get_end_effector_link()
                        
        # Set the reference frame for pose targets
        reference_frame = 'base_link'
        
        # Set the right arm reference frame accordingly
        arm.set_pose_reference_frame(reference_frame)
                
        # Allow replanning to increase the odds of a solution
        arm.allow_replanning(True)
        
        # Allow some leeway in position (meters) and orientation (radians)
        arm.set_goal_position_tolerance(0.01)
        arm.set_goal_orientation_tolerance(0.05)
        
        # Start the arm in the "resting" pose stored in the SRDF file
        rospy.loginfo("execute vertical")
        arm.set_named_target('vertical')
        arm.go()
        rospy.sleep(1)

        rospy.loginfo("execute retract")
        arm.set_named_target('retract')
        arm.go()
        rospy.sleep(1)

        target_pose = PoseStamped()
        target_pose.header.frame_id = reference_frame
        target_pose.header.stamp = rospy.Time.now()     

        target_pose.pose.position.x = 0.0
        target_pose.pose.position.y = 0.0
        target_pose.pose.position.z = 0.0
        target_pose.pose.orientation.x = 0.0
        target_pose.pose.orientation.y = 0.0
        target_pose.pose.orientation.z = 0.0
        target_pose.pose.orientation.w = 1.0

        #rospy.loginfo(position.x, position.y, position.z, orientation.x, orientation.y, orientation.z )
        
        # Set the start state to the current state
        arm.set_start_state_to_current_state()
        
        # Set the goal pose of the end effector to the stored pose
        arm.set_pose_target(target_pose, end_effector_link)
        
        # Plan the trajectory to the goal
        traj = arm.plan()
        
        # Execute the planned trajectory
        rospy.loginfo("execute traj")
        arm.execute(traj)

        rospy.sleep(1)
         
        arm.shift_pose_target(0, 0.2, end_effector_link)
        rospy.loginfo("execute target1")
        arm.go()
        rospy.sleep(1)

        arm.shift_pose_target(1, 0.1, end_effector_link)
        rospy.loginfo("execute target2")
        arm.go()
        rospy.sleep(1)

        arm.shift_pose_target(2, 0.2, end_effector_link)
        rospy.loginfo("execute target3")
        arm.go()
        rospy.sleep(1)

        # arm.shift_pose_target(3, -0.78, end_effector_link)
        # rospy.loginfo("execute target4")
        # arm.go()
        # rospy.sleep(1)

        arm.shift_pose_target(4, -0.78, end_effector_link)
        rospy.loginfo("execute target4")
        arm.go()
        rospy.sleep(1)

        # arm.shift_pose_target(5, 0.39, end_effector_link)
        # rospy.loginfo("execute target5")
        # arm.go()
        # rospy.sleep(1)
          
        # Store this pose as the new target_pose
        saved_target_pose = arm.get_current_pose(end_effector_link)
          
        # Move to the named pose "home"
        arm.set_named_target('retract')
        rospy.loginfo("execute retract")
        arm.go()
        rospy.sleep(1)

        # Shut down MoveIt cleanly
        moveit_commander.roscpp_shutdown()
        
        # Exit MoveIt
        moveit_commander.os._exit(0)

if __name__ == "__main__":
    MoveItDemo()

    
    